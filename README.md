# twitter-clone-go

Go twitter clone REST API

```sh
docker run --ulimit memlock=-1:-1 -it --rm=true --memory-swappiness=0 \
    --name twitter-clone -e POSTGRES_USER=twitter \
    -e POSTGRES_PASSWORD=twitter -e POSTGRES_DB=twitter \
    -p 5432:5432 postgres:latest
```