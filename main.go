package main

import (
	"net/http"

	"github.com/drouian-m/twitter-clone-go/tweet"
	"github.com/gin-contrib/cors"
	"github.com/gin-gonic/gin"
	"gorm.io/driver/postgres"
	"gorm.io/gorm"
)

func main() {
	gin.SetMode(gin.ReleaseMode)

	router := gin.Default()
	router.Use(cors.Default())

	dsn := "host=localhost user=twitter password=twitter dbname=twitter port=5432 sslmode=disable"
	db, err := gorm.Open(postgres.Open(dsn), &gorm.Config{})
	if err != nil {
		panic("failed to connect database")
	}

	tweetService := tweet.NewTweetService(db)

	router.GET("/tweets", func(c *gin.Context) {
		tweets := tweetService.ListTweets()
		c.JSON(http.StatusOK, tweets)
	})

	router.POST("/tweets", func(c *gin.Context) {
		var input tweet.TweetRequest
		if err := c.ShouldBindJSON(&input); err != nil {
			c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
			return
		}
		tweets := tweetService.CreateTweet(input)
		c.JSON(http.StatusOK, tweets)
	})

	router.Run(":8082")
}
